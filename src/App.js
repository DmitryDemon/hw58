import React, { Component } from 'react';
import Modal from './components/UI/Modal/Modal';
import Alert from './components/UI/Alert/Alert';
import './App.css';

class App extends Component {

    state = {
        purchasable: false,
        purchasing: false,
    };

    purchaseCancel = () => {
        this.setState({purchasing: false});
    };

    purchase = () => {
        this.setState({purchasing: true});
    };

    someHandler = () => {

    };



  render() {
    return (
      <div className="App">
          {this.state.purchasing &&  <Modal
              closed={this.purchaseCancel}
              title={'Some kinda modal title'}
          >
              <p>This is modal content</p>
          </Modal>}
        <button onClick={this.purchase}>Show modal</button>
          <Alert

              type='warning'

              dismiss={this.someHandler}

          >This is a warning type alert</Alert>

          <Alert type="success">This is a success type alert</Alert>
          <Alert type="primary">This is a success type alert</Alert>
          <Alert type="danger">This is a success type alert</Alert>
      </div>
    );
  }
}

export default App;
