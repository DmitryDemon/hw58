import React, {Fragment} from 'react';
import './Alert.css'
const Alert = props => {
    return (
        <Fragment>
            <div className={['Alert', props.type].join(' ')} >
                {props.dismiss !== undefined ? <button className='alertClose' onClick={props.dismiss}>Закрыть</button> : null}
                <p>{props.children}</p>
            </div>
        </Fragment>

    );
};

export default Alert;
// className={['Button', props.btnType].join(' ')}