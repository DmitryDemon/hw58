import React, {Fragment} from 'react';
import './Modal.css';


const Modal = props => {
  return (

    <Fragment>

      <div
        className="Modal">
          <h3>{props.title}<button className='btnClose' onClick={props.closed}>X</button></h3>
        {props.children}
      </div>
    </Fragment>
  );
};

export default Modal;